<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'id' => '1',
                    'name' => 'aa',
                    'email' => 'a@a.com',
                    'password' =>  Hash::make('12345678'),
                    'role' => 'manager',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'id' => '2',
                    'name' => 'bb',
                    'email' => 'b@b.com',
                    'password' =>  Hash::make('12345678'),
                    'role' => 'salesrep',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'id' => '3',
                    'name' => 'cc',
                    'email' => 'c@c.com',
                    'password' =>  Hash::make('12345678'),
                    'role' => 'salesrep',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
            ]);
    }
}
