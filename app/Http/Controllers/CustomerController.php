<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        return view('customers.index', ['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customers= new Customer();
        $id = Auth::id();
        $customers->name = $request->name;
        $customers->email = $request->email;
        $customers->fhone = $request->fhone;
        $customers->user_id=$id;
        $customers->save();
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customers = Customer::find($id);
        return view('customers.edit', compact('customers'));
    }

   /* :(  Question 5-> I was unable to retrieve the user_id from the client table and compare it to the id of the users table :(
    public function edit($id, $user_id )
    {
        $customers = Customer::find($user_id);
        $user = User::find($id);
        if (Gate::denies('manager')) {
             if ($id!=$user_id){
            abort(403,"Sorry you are not allowed to delete..");}}
        $customers = Customer::find($id);
        return view('customers.edit', compact('customers'));
    }
    */ 

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customers = Customer::find($id);
        $customers->update($request->all());
        return redirect('customers'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to delete..");
         }
        $customers = Customer::find($id);
        $customers->delete();
        return redirect('customers');    }


        public function changeStatus($id)
        {
            //only if this todo belongs to user 
            if (Gate::denies('manager')) {
                abort(403,"You are not allowed to mark tasks as dome..");
             }          
            $customers = Customer::findOrFail($id);            
            $customers->status = 1; 
            $customers->save();
            return redirect('customers');    
        }  
}
