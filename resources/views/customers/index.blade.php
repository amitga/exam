@yield('content')

@extends('layouts.app')
@section('content')

<h1> This is the customers list</h1>

<a href = "{{route('customers.create')}}"> Create new customer</a>

<table>
<tr>
    <th>Customers name (user id)</th>
    <th>Customers email</th>
    <th>Customers fhone number</th>
    <th>Edit</th>
    <th>Delete</th> 
    @can('manager') <th>Deal closed</th> @endcan
</tr>
    @foreach($customers as $customer)
<tr>
    <td>  {{$customer->name}}  ({{$customer->user_id}}) </td> <!--I could not display the user name but I was able to display is id --> 
    <td> {{$customer->email}}</td>
    <td> {{$customer->fhone}}</td>
    <td> <a href="{{route('customers.edit',$customer->id )}}"> Edit </a> </td>  
    <td> @can('manager')   <!--things only manager can see  -->  

    <form method = 'post' action = "{{action('CustomerController@destroy', $customer->id)}}" > 

    @csrf
    @method('DELETE')

    <div class = "form-group">
        <input type = "submit" class = "form-control" name = "submit" value = "Delete">
    </div>
    @endcan <!--manager  --> 
    @can('salesrep') <!--things only salesrep can see -->  Delete @endcan <!--salesrep  --> </td>


    @can('manager')<td>  @if ($customer->status == 0)
    <a href="{{route('changeStatus',$customer->id)}}"> Deal closed </a> </td>
    @else
    
    @endif </td> @endcan
    </form>
    
</tr>
    @endforeach
    </table>


    @endsection

    <style>
table, th, td {
  border: 1px solid black;
}
    </style>




  
